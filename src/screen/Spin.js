import React from 'react';
import '../CSScomponent/Spin.css';
import Popup from '../component/Popup';
import Turn from '../component/Turn';
import Foot from '../component/Foot';
import PopupBuyAutoSpin from '../component/PopupBuyAutoSpin';
import { Link } from 'react-router-dom';

export default class AppTest extends React.Component {
    constructor(props) {
        super(props);
        const arr = [1805, 1825, 1845, 1865];
        const randIndex = Math.floor(Math.random() * arr.length);
        this.state = {
            rotation: 0,
            time: arr[randIndex],
            showPopup: false,
            vnd: 40,
            showAutoPopup: false,
            isSpinning: false,
            marginBottom: 1,
            opacityPopup: 0,
            numDay:30,
        //asdas
        }
        this.spinTimer = null;
        this.rotate = this.rotate.bind(this);
    }

    componentDidUpdate = () => {
        if (this.state.showPopup === true) {
            clearInterval(this.opacityPopupTimer);
            this.opacityPopupTimer = setInterval(() => { this.checkopacityPopupTimer() }, 30);
        }
    }

    checkopacityPopupTimer = () => {
        if (this.state.opacityPopup > 1) {
            clearInterval(this.opacityPopupTimer);
        } else {
            this.setState({ opacityPopup: this.state.opacityPopup + 0.3 })
        }
        console.log(this.state.opacityPopup)
    }

    togglePopup() {
        if (this.state.showPopup === false) {
            this.setState({ opacityPopup: 0 })
        }
        this.setState({
            showPopup: !this.state.showPopup,
        });

    }

    togglePopupBuy() {
        this.setState({
            showAutoPopup: !this.state.showAutoPopup
        });
    }

    rotate() {
        this.setState({ isSpinning: true });
        const time = this.state.time;
        let newRotation = (this.state.rotation + time / 50) % 360;

        if (this.state.time < 0) {
            clearInterval(this.spinTimer);
            this.stopSpinWheel();
        } else {
            if (this.state.time < 400) {
                this.setState({ marginBottom: this.state.marginBottom + 0.1 })
                if (this.state.marginBottom > 1) {
                    this.setState({ marginBottom: 1 });
                }
            } else {
                this.setState({ marginBottom: this.state.marginBottom - 0.1 })
                if (this.state.marginBottom < 0) {
                    this.setState({ marginBottom: 0 });
                }
            }

            this.setState({
                rotation: newRotation,
                time: this.state.time - 10,
            },
                () => {
                    // console.log(this.state.marginBottom)
                    clearInterval(this.spinTimer);
                    this.spinTimer = setInterval(() => { this.rotate() }, 20);
                })
        }
    }

    stopSpinWheel = () => {
        this.setState({ isSpinning: false })
        const rotation = this.state.rotation;
        //console.log(rotation);
        if ((0 <= rotation && rotation < 18) || rotation >= 342) {
            alert('5000 apoint')
        } else if (18 <= rotation && rotation < 54) {
            const arr = [1815, 1835, 1855];
            const randIndex = Math.floor(Math.random() * arr.length);
            this.setState({ time: arr[randIndex], rotation: 36 });
            this.setState({
                TextTitlePopup: 'VÒNG QUAY MAY MẮN', TextFootPopup: 'Luật chơi', isChooseNum: false,
                titleText: 'Chúc mừng! Bạn đã nhận được', contentText: '1 Lượt quay', isBuyTurn: false
            }, this.togglePopup())
        } else if (54 <= rotation && rotation < 90) {
            alert('2000 apoint')
        } else if (90 <= rotation && rotation < 126) {
            const arr = [1815, 1835, 1795, 1775];
            const randIndex = Math.floor(Math.random() * arr.length);
            this.setState({ time: arr[randIndex], rotation: 108 });
            this.setState({
                TextTitlePopup: 'VÒNG QUAY MAY MẮN', TextFootPopup: 'Luật chơi', isBuyTurn: false, isChooseNum: true,
                titleText: 'Chọn số may mắn để tham gia nhận thưởng Apoint', contentText: ''
            }, this.togglePopup())
        } else if (126 <= rotation && rotation < 162) {
            alert('1000 apoint')
        } else if (162 <= rotation && rotation < 198) {
            const arr = [1815, 1755, 1795, 1775];
            const randIndex = Math.floor(Math.random() * arr.length);
            this.setState({ time: arr[randIndex], rotation: 180 });
            this.setState({
                TextTitlePopup: 'VÒNG QUAY MAY MẮN', TextFootPopup: 'Luật chơi', isBuyTurn: false, isChooseNum: false,
                titleText: 'Bạn nhận được 1 số may mắn', contentText: '56'
            }, this.togglePopup())
        } else if (198 <= rotation && rotation < 234) {
            alert('500 apoint')
        } else if (234 <= rotation && rotation < 270) {
            const arr = [1835, 1755, 1795, 1775];
            const randIndex = Math.floor(Math.random() * arr.length);
            this.setState({ time: arr[randIndex], rotation: 252 });
            this.setState({
                TextTitlePopup: 'VÒNG QUAY MAY MẮN', TextFootPopup: 'Luật chơi', isChooseNum: false,
                titleText: 'Chúc mừng! Bạn đã nhận được', contentText: '3 Lượt quay', isBuyTurn: false
            }, this.togglePopup())
        } else if (270 <= rotation && rotation < 306) {
            alert('100 apoint')
        } else if (306 <= rotation && rotation < 342) {
            alert('Voucher')
        }
    }

    AutoSpin = () => {
        this.setState({ showPopup: !this.state.showPopup, showAutoPopup: !this.state.showAutoPopup })
    }
    render() {
        const { rotation, marginBottom, opacityPopup } = this.state;
        const opacityX = this.state.showPopup || this.state.showAutoPopup ? 0.2 : 1;
        return (
            <div class='bodySP' >
                <div class='containerRuleSP' style={{ opacity: opacityX }}>
                    <div class='headRuleSP' >
                        {/* <Link to='/'> */}
                            <div class='centerSP' style={{ color: "white", fontSize: 30 }}>x</div>
                        {/* </Link> */}
                        <div class='centerSP ruleSP'></div>
                    </div>
                    <div class='KQ' style={{ opacity: marginBottom }}>
                        {/* <Link className='link' to='/'> */}
                            <div class='itemKQ'>
                                <div class='whiteTextAS '>Kết quả 16/05: 31</div>
                                <div class='yellowTextAS'>20.150 Apoint</div>
                            </div>
                        {/* </Link> */}
                        <div class='borderKQ'/>
                        <div class='itemKQ'>
                            <div class='whiteTextAS'>Giải thưởng 17/05</div>
                            <div class='yellowTextAS'>20.150 Apoint</div>
                        </div>
                    </div>
                    <div class='SpinAround'>
                        <img class='imageSpin' alt='asd' style={{ transform: `rotate(${rotation}deg)` }}
                            src={require('../icon/spin_wheel.webp')} width="300" height='300' />
                    </div>
                    <div class='viewPlay'>
                        <img class='imageIconSP' onClick={() => this.rotate()} alt='asd'
                            src={require('../icon/btn_play_center.webp')}
                            width="75" height='75' />
                    </div>
                    <div class='FootViewAS' style={{ opacity: marginBottom }}>
                        <Turn onClick={() => this.setState({
                            TextTitlePopup: 'MUA THÊM LƯỢT',
                            TextFootPopup: 'Mua gói quay tự động', titleText: '', contentText: '', isBuyTurn: true, isChooseNum: false,
                        }, this.togglePopup())} />
                        <Foot />
                    </div>
                </div>

                {/* <div class='Popup'> */}
                    {this.state.showPopup ?
                        <div class='viewPopup' style={{ opacity: opacityPopup }} >
                            <Popup
                                timeSpin={this.state.time}
                                ref='PopupScreen'
                                TextTitlePopup={this.state.TextTitlePopup}
                                TextFootPopup={this.state.TextFootPopup}
                                titleText={this.state.titleText}
                                contentText={this.state.contentText}
                                isBuyTurn={this.state.isBuyTurn}
                                isChooseNum={this.state.isChooseNum}
                                closePopup={() => this.togglePopup()}
                                vnd={this.state.vnd}
                                AutoSpin={() => this.AutoSpin()}
                                vnd20Function={() => this.setState({ vnd: 20 })}
                                vnd40Function={() => this.setState({ vnd: 40 })}
                                vnd60Function={() => this.setState({ vnd: 60 })}
                            />
                        </div>
                        : null
                    }
                    {this.state.showAutoPopup ?
                        <div class='viewPopup' style={{ top: 50 }}>
                            <PopupBuyAutoSpin
                                closePopup={() => this.togglePopupBuy()}
                                numDay={this.state.numDay}
                                buyTurn={() => this.setState({
                                    showAutoPopup: !this.state.showAutoPopup, showPopup: !this.state.showPopup,
                                    TextTitlePopup: 'MUA THÊM LƯỢT', TextFootPopup: 'Mua gói quay tự động', titleText: '',
                                    contentText: '', isBuyTurn: true, isChooseNum: false,
                                })}
                                day7Function={() => this.setState({ numDay: 7 })}
                                day30Function={() => this.setState({ numDay: 30 })}
                                day90Function={() => this.setState({ numDay: 90 })}
                            />
                        </div>
                        : null
                    }
                {/* </div> */}
            </div>
        )
    }
}