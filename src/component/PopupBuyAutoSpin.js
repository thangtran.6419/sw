import React from 'react';
import '../CSScomponent/PopupBuyAutoSpin.css';
// import { Link } from 'react-router-dom';

class PopupBuyAutoSpin extends React.Component {

    buyTurnUI = () => {
        const color7 = this.props.numDay === 7 ? 'white' : null;
        const color30 = this.props.numDay === 30 ? 'white' : null;
        const color90 = this.props.numDay === 90 ? 'white' : null;

        return (
            <div class='contentTextAT buyturnAT' >
                <div onClick={this.props.day7Function} class='itemBuyTurnAT'>
                    {this.props.numDay === 7
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="83" height='52' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurnAT center'>
                        <div class='yellowTextAS fontsize' style={{ color: color7 }}>7 NGÀY</div>
                        <div class=' whiteTextAS fontsize'>50.000 VNĐ</div>
                    </div>
                </div>
                <div onClick={this.props.day30Function} class='itemBuyTurnAT'>
                    {this.props.numDay === 30
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="83" height='52' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurnAT' style={{ marginLeft: -5 }}>
                        <div class='yellowTextAS fontsize' style={{ color: color30 }}>30 NGÀY</div>
                        <div class='whiteTextAS fontsize'>150.000 VNĐ</div>
                    </div>
                </div>
                <div onClick={this.props.day90Function} class='itemBuyTurnAT'>
                    {this.props.numDay === 90
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="83" height='52' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurnAT' style={{ marginLeft: -5 }}>
                        <div class='yellowTextAS fontsize' style={{ color: color90 }}>90 NGÀY</div>
                        <div class='whiteTextAS fontsize'>400.000 VNĐ</div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className='popupAT' style={{ opacity: 1 }}>
                <div className='popupInnerAT'>
                    <div class='viewHeadImageAT'>
                        <img class='headImageAT' src={require('../icon/head_pop.webp')} alt='asd' style={{ marginLeft: 0, marginTop: 0 }} width="320" height='50' />
                    </div>
                    <div class='titleTextAT'>MUA NGAY GÓI QUAY TỰ ĐỘNG</div>
                    <div class='close_popupAT' onClick={this.props.closePopup} />
                </div>
                <div className='popupInnerAT'>
                    <div>
                        <img src={require('../icon/bg.webp')} alt='asd' width="320" height='520' />
                    </div>
                    <div class='titleTextAT topAT flexStart'>Chọn gói quay</div>
                    {this.buyTurnUI()}
                    <div class=' titleTextAT topAT flexStart' style={{ marginTop: 90 }} >Chọn 5 số may mắn</div>
                    <div class=' contentTextAT inputNumberAT' style={{ marginTop: 15 }} >
                        <div class='oneInputAT'>
                        </div>
                        <div class='oneInputAT'>
                        </div>
                        <div class='oneInputAT'>
                        </div>
                        <div class='oneInputAT'>
                        </div>
                        <div class='oneInputAT'>
                        </div>
                    </div>
                    <div class='titleTextAT topAT flexStart' style={{ marginTop: 175 }}>Chọn giờ quay</div>
                    <div class='oneInputAT contentTextAT' style={{ width: 280, marginTop: 100, color: 'white', fontSize: 20, justifyContent: 'flex-start', alignItems: 'center' }}>
                        <div style={{ marginLeft: 10, marginTop: 5 }}>14:30</div>
                    </div>
                    <div class=' titleTextAT topAT center' style={{ marginLeft: -20, marginTop: 260, fontSize: 15, flexDirection: 'row' }} >
                        <div style={{ color: 'orange', marginTop: -25 }}>*</div>
                        <div style={{ color: 'white' }}>Hệ thống sẽ tự động quay hết lượt quay ngày và gian quay tự động được tính vào ngày mai</div>
                    </div>
                    <div class='titleTextAT topAT SettingViewAT' style={{ marginTop: 285, flexDirection: 'row', marginLeft: -40 }}>
                        <div class='textSoundPPAT whiteText'>Quay lượt quay chính</div>
                        <div class='switchViewAT'>
                            <label class="switchAT">
                                <input type="checkbox" />
                                <span class="sliderAT roundAT"></span>
                            </label>
                        </div>
                    </div>
                    <div class='titleTextAT topAT SettingViewAT' style={{ marginTop: 320, flexDirection: 'row', marginLeft: -40 }}>
                        <div class='textSoundPPAT whiteText'>Tự động gia hạn</div>
                        <div class='switchViewAT'>
                            <label class="switchAT">
                                <input type="checkbox" />
                                <span class="sliderAT roundAT"></span>
                            </label>
                        </div>
                    </div>
                    <div class='contentTextAT'>{this.props.contentText}</div>
                    <div onClick={this.props.closePopup} class='btn_submitAT'>
                        <img onClick={this.props.closePopup} src={require('../icon/btn_yellow.webp')} alt='asd' width="280" height='50' />
                        <div onClick={this.props.closePopup} class='text_submitAT'>XÁC NHẬN</div>
                    </div>
                    <div class='luatchoiAT' onClick={this.props.buyTurn}>Mua lượt ngay</div>
                </div>
                <div className='popupInnerAT footImage' style = {{marginTop: 50}}>
                    <img src={require('../icon/foot_pop.webp')} alt='asd' width="320" height='20' />
                </div>
            </div>
        );
    }
}

export default PopupBuyAutoSpin;

///sklflakj